from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator


"""
- Ingredient(ингредиент)
- Order(заказ)
"""


class Dish(models.Model):
    name = models.CharField(max_length=50, unique=True)
    ingredients = models.ManyToManyField(
        'Ingredient',
        through='DishIngredient',
        related_name='dishes',
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        return self.name


class Ingredient(models.Model):
    name = models.CharField(max_length=50, unique=True)

    def __str__(self):
        return self.name

